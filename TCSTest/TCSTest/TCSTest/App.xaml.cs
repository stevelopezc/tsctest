﻿using Prism;
using Prism.Ioc;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using TCSTest.Views;
using TCSTest.ViewModels;
using Acr.UserDialogs;
using TCSTest.Data;
using System.Linq;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace TCSTest
{
    public partial class App
    {
        /* 
         * The Xamarin Forms XAML Previewer in Visual Studio uses System.Activator.CreateInstance.
         * This imposes a limitation in which the App class must have a default constructor. 
         * App(IPlatformInitializer initializer = null) cannot be handled by the Activator.
         */
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }

        protected override async void OnInitialized()
        {
            InitializeComponent();

            string initialPage = "NavigationPage/Main";
            var dataService = DataService.GetInstanceRealm();
            var sesion = dataService.All<Sesion>().FirstOrDefault();
            if (sesion != null && !string.IsNullOrEmpty(sesion.UserConnected))
                initialPage = "NavigationPage/Countries";

            await NavigationService.NavigateAsync(initialPage);
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterInstance<IUserDialogs>(UserDialogs.Instance);

            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<Main, MainViewModel>();
            containerRegistry.RegisterForNavigation<Countries, CountriesViewModel>();
            containerRegistry.RegisterForNavigation<Country, CountryViewModel>();
            containerRegistry.RegisterForNavigation<SubDivisions, SubDivisionsViewModel>();
            containerRegistry.RegisterForNavigation<SubDivision, SubDivisionViewModel>();
        }
    }
}
