﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TCSTest.Data
{
    public class Sesion : Realms.RealmObject
    {
        public string UserConnected { get; set; }
        public DateTimeOffset LoginDate { get; set; }
    }
}
