﻿using Plugin.Settings;
using Plugin.Settings.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace TCSTest.Helpers
{
    public class Settings
    {
        private static ISettings AppSettings => CrossSettings.Current;

        public static string URLApiBase
        {
            get => AppSettings.GetValueOrDefault(nameof(URLApiBase), "https://exam-api.tsc-dev.xyz");
            set => AppSettings.AddOrUpdateValue(nameof(URLApiBase), value);
        }

        public static string URLPrefix
        {
            get => AppSettings.GetValueOrDefault(nameof(URLPrefix), "/api");
            set => AppSettings.AddOrUpdateValue(nameof(URLPrefix), value);
        }
    }
}
