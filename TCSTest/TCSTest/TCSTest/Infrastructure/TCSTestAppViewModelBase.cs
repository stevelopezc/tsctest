﻿using Acr.UserDialogs;
using Prism.Commands;
using Prism.Navigation;
using Realms;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TCSTest.Data;
using TCSTest.Services;
using Xamarin.Essentials;

namespace TCSTest.Infrastructure
{
    public class TCSTestAppViewModelBase : AppMapViewModelBase
    {
        private bool _isRunning = false;
        private readonly INavigationService navigationService;
        private readonly IUserDialogs userDialogs;

        public bool IsRunning {
            get => _isRunning;
            set {
                if (_isRunning != value)
                {
                    _isRunning = value;
                    SetProperty(ref _isRunning, value);
                    ShowLoading(_isRunning);
                }
            }
        }

        public DelegateCommand LogOutCommand { get; set; }
        public DelegateCommand GoBackCommand { get; set; }

        public Realm context { get; }

        public bool IsConnected { get { return Connectivity.NetworkAccess == NetworkAccess.Internet; } }
        public TCSTestAppViewModelBase(INavigationService navigationService, IUserDialogs userDialogs, ApiService apiService) : base(navigationService)
        {
            this.navigationService = navigationService;
            this.userDialogs = userDialogs;

            LogOutCommand = new DelegateCommand(async () => await logout());
            GoBackCommand = new DelegateCommand(async () => await goBack());

            context = DataService.GetInstanceRealm();
        }

        private async Task logout()
        {
            context.Write(() =>
            {
                context.RemoveAll<Sesion>();
            });
            await navigationService.NavigateAsync("/Main");
        }

        private async Task goBack()
        {
            await navigationService.GoBackAsync(animated: true);
        }

        private void ShowLoading(bool isRunning)
        {
            if (isRunning)
                userDialogs.ShowLoading("Processing..." );
            //await navigationService.NavigateAsync("LoadingPage?message=" + (string.IsNullOrEmpty(ProcessMessage) ? Languages.Processing : ProcessMessage));
            else
            {
                //await navigationService.GoBackAsync();
                userDialogs.HideLoading();
            }
        }
    }
}
