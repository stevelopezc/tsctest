﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace TCSTest.Models
{
    public class Country
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("alpha2")]
        public string Alpha2 { get; set; }

        [JsonProperty("alpha3")]
        public string Alpha3 { get; set; }

        [JsonProperty("code")]
        public int? Code { get; set; }

        [JsonProperty("iso_3166_2")]
        public string Iso3166_2 { get; set; }

        [JsonProperty("is_independent")]
        public bool IsIndependent { get; set; }

        [JsonProperty("created_at")]
        public DateTimeOffset CreatedAt { get; set; }

        [JsonProperty("updated_at")]
        public DateTimeOffset UpdatedAt { get; set; }

        public override int GetHashCode()
        {
            return Id;
        }
    }
}
