﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TCSTest.Models
{
    public class ErrorModel
    {
        [JsonProperty("error")]
        public bool Error { get; set; }

        [JsonProperty("errorCode")]
        public long ErrorCode { get; set; }

        [JsonProperty("errorMessages")]
        public List<string> ErrorMessages { get; set; }

        public string ErrorDescription { get { return ErrorMessages.Count==0?"Errors": ErrorMessages.Aggregate((v1, v2)=> $"{v1};{v2}");  } }
    }
}
