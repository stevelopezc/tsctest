﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace TCSTest.Models
{
    public class ResponseCountryAPIModel
    {
        [JsonProperty("data")]
        public List<Country> Data { get; set; }
    }

    public class ResponseSubDivisionAPIModel
    {
        [JsonProperty("data")]
        public List<SubDivision> Data { get; set; }
    }
}
