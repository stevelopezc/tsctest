﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TCSTest.Helpers;
using Utilities.RestService;
using Newtonsoft.Json;

namespace TCSTest.Services
{
    public class ApiService
    {
        public ApiService()
        {

        }

        public async Task<Response> GetList<T>(string controller, bool IsSecure)
        {
            try
            {
                var restClient = new ServiceClient
                {
                    URLBase = Settings.URLApiBase,
                };


                var response = await restClient.GetList<T>(controller, IsSecure);

                return new Response
                {
                    Is_Error = false,
                    Result = response
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    Is_Error = true,
                    Result = "Error: " + ex.Message
                };
            }
        }

        public async Task<Response> Get<T>(string controller, bool IsSecure, string param = "", string valorParam = "", int? ModelId = null)
        {

            try
            {
                var restClient = new ServiceClient
                {
                    URLBase = Settings.URLApiBase,
                };


                var paramList = string.Empty;
                if (!string.IsNullOrEmpty(param))
                {
                    string sTemp = string.Empty;
                    string[] sParam = param.Split('^');
                    string[] sVal = valorParam.Split('^');
                    for (int i = 0; i < sParam.Length; i++)
                        restClient.Parameters.Add(sParam[i], sVal[i]);
                }
                restClient.ModelId = ModelId;

                var response = await restClient.Get<T>(controller, IsSecure);

                return new Response
                {
                    Is_Error = false,
                    Result = response
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    Is_Error = true,
                    Message = "Error: " + ex.Message
                };
            }
        }
        public async Task<Response> Post<T>(string controller, T model, bool IsSecure, bool IsApi = true)
        {
            try
            {

                var restClient = new ServiceClient
                {
                    URLBase = Settings.URLApiBase,
                };

                var response = await restClient.Post<T>(controller, model, true);

                return new Response
                {
                    Is_Error = false,
                    Result = response
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    Is_Error = true,
                    Message = ex.Message,
                };
            }
        }

        public async Task<Response> PostRequest<TIn, TOut>(string controller, TIn model, bool IsSecure, bool IsApi = true)
        {
            try
            {

                var restClient = new ServiceClient
                {
                    URLBase = Settings.URLApiBase,
                };

                var response = await restClient.PostRequest<TIn, TOut>(controller, model, true);

                return new Response
                {
                    Is_Error = false,
                    Result = response
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    Is_Error = true,
                    Message = ex.Message,
                };
            }
        }

        public async Task<Response> Put<T>(string controller, T model, bool IsSecure)
        {
            try
            {

                var restClient = new ServiceClient
                {
                    URLBase = Settings.URLApiBase,
                };


                var response = await restClient.Put<T>(controller, model.GetHashCode().ToString(), model, true);
                return new Response
                {
                    Is_Error = false,
                    Result = response
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    Is_Error = true,
                    Message = ex.Message,
                };
            }
        }

        public async Task<Response> Patch<T>(string controller, T model, bool IsSecure)
        {
            try
            {

                var restClient = new ServiceClient
                {
                    URLBase = Settings.URLApiBase,
                };


                var response = await restClient.Patch<T>(controller, model.GetHashCode().ToString(), model, true);
                return new Response
                {
                    Is_Error = false,
                    Result = response
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    Is_Error = true,
                    Message = ex.Message,
                };
            }
        }

        public async Task<Response> Delete<T>(
            string controller,
            bool IsSecure,
            T model)
        {
            try
            {
                var restClient = new ServiceClient
                {
                    URLBase = Settings.URLApiBase,
                };

                var response = await restClient.Delete<T>(controller, model.GetHashCode().ToString(), true);

                return new Response
                {
                    Is_Error = false,
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    Is_Error = true,
                    Message = ex.Message,
                };
            }
        }

    }
}
