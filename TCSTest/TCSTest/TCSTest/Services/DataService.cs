﻿using Realms;
using System;
using System.Collections.Generic;
using System.Text;

namespace TCSTest.Data
{
    public static class DataService
    {
        public static Realm GetInstanceRealm()
        {
            var config = new RealmConfiguration("TCSTest.realm")
            {
                SchemaVersion = 1

            };

            var _realm = Realm.GetInstance(config);

            return _realm;
        }
    }
}
