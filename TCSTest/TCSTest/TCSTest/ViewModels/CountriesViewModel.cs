using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Prism;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using TCSTest.Infrastructure;
using TCSTest.Models;
using TCSTest.Services;

namespace TCSTest.ViewModels
{
    public class CountriesViewModel : TCSTestAppViewModelBase
    {
        private readonly INavigationService navigationService;
        private readonly IUserDialogs userDialogs;
        private readonly ApiService apiService;

        private ObservableCollection<Country> _countriesList;
        private Country _SelectedCountry;
        private bool _IsSelected;

        public DelegateCommand NewCommand { get; set; }
        public DelegateCommand EditCommand { get; set; }
        public DelegateCommand DeleteCommand { get; set; }
        public DelegateCommand DetailCommand { get; set; }

        public CountriesViewModel(INavigationService navigationService, IUserDialogs userDialogs, ApiService apiService) : base(navigationService, userDialogs, apiService)
        {
            this.navigationService = navigationService;
            this.userDialogs = userDialogs;
            this.apiService = apiService;
            NewCommand = new DelegateCommand(async () => await GoToNew());
            EditCommand = new DelegateCommand(async () => await GoToEdit());
            DeleteCommand = new DelegateCommand(async () => await Delete());
            DetailCommand = new DelegateCommand(async () => await GoToDetails());
        }

        public ObservableCollection<Country> CountriesList { get => _countriesList; set => SetProperty(ref _countriesList , value); }
        public Country SelectedCountry { get => _SelectedCountry; set => SetProperty(ref _SelectedCountry , value); }
        public bool IsSelected { get => _IsSelected; set => SetProperty(ref _IsSelected , value); }

        public async override void OnNavigatedTo(INavigationParameters parameters)
        {
            await LoadData();
        }

        private async Task LoadData()
        {
            if (!IsConnected)
            {
                userDialogs.Toast(new ToastConfig("There was an error getting data").SetDuration(TimeSpan.FromSeconds(30)).SetPosition(ToastPosition.Bottom));
                return;
            }

            IsRunning = true;
            var response = await apiService.Get<ResponseCountryAPIModel>("countries", false);
            IsRunning = false;

            if (response.Is_Error)
            {
                var error = JsonConvert.DeserializeObject<ErrorModel>(response.Message);
                userDialogs.Toast(new ToastConfig(error.ErrorDescription).SetDuration(TimeSpan.FromSeconds(30)).SetPosition(ToastPosition.Bottom));
                return;
            }

            CountriesList = new ObservableCollection<Country>((response.Result as ResponseCountryAPIModel).Data);
        }

        public async Task GoToDetails()
        {
            NavigationParameters navParameters = new NavigationParameters();
            navParameters.Add("Country", SelectedCountry);
            await navigationService.NavigateAsync("SubDivisions", navParameters);
        }

        public async Task GoToEdit()
        {
            NavigationParameters navParameters = new NavigationParameters
            {
                { "Country", SelectedCountry }
            };
            await navigationService.NavigateAsync("Country", navParameters);
        }

        public async Task GoToNew()
        {
            await navigationService.NavigateAsync("Country");
        }

        public async Task Delete()
        {
            var confirm = await userDialogs.ConfirmAsync(new ConfirmConfig { Message = "Are you sure to Delete this Country?", OkText = "Ok", CancelText = "Cancel" });
            if (confirm)
            {
                if (!IsConnected)
                {
                    userDialogs.Toast(new ToastConfig("There was an error getting data").SetDuration(TimeSpan.FromSeconds(30)).SetPosition(ToastPosition.Bottom));
                    return;
                }

                IsRunning = true;
                var response = await apiService.Delete("countries",false, SelectedCountry);
                IsRunning = false;

                if (response.Is_Error)
                {
                    var error = JsonConvert.DeserializeObject<ErrorModel>(response.Message);
                    await userDialogs.AlertAsync(new AlertConfig { Message = error.ErrorDescription, OkText = "Ok", Title = "Error" });
                    return;
                }

                await LoadData();
            }
        }
    }
}
