using System;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Prism;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using TCSTest.Infrastructure;
using TCSTest.Models;
using TCSTest.Services;

namespace TCSTest.ViewModels
{
    public class CountryViewModel : TCSTestAppViewModelBase
    {
        private Country _SelectedItem;
        private string _Action;
        private readonly INavigationService navigationService;
        private readonly IUserDialogs userDialogs;
        private readonly ApiService apiService;

        
        public DelegateCommand SaveCommand { get; set; }
        public Country SelectedItem { get => _SelectedItem; set => SetProperty(ref _SelectedItem, value); }
        public string Action { get => _Action; set => SetProperty(ref _Action , value); }

        public CountryViewModel(INavigationService navigationService, IUserDialogs userDialogs, ApiService apiService) : base(navigationService, userDialogs, apiService)
        {
            SelectedItem = new Country();
            Action = "New";
            
            SaveCommand = new DelegateCommand(async () => await save());
            this.navigationService = navigationService;
            this.userDialogs = userDialogs;
            this.apiService = apiService;
        }

        private async Task save()
        {
            if (string.IsNullOrEmpty(SelectedItem.Name))
            {
                await userDialogs.AlertAsync(new AlertConfig { Message = "Invalid Name", OkText = "Ok", Title = "Error" });
                return;
            }

            if (string.IsNullOrEmpty(SelectedItem.Alpha2))
            {
                await userDialogs.AlertAsync(new AlertConfig { Message = "Invalid Code (2 chars)", OkText = "Ok", Title = "Error" });
                return;
            }
          
            IsRunning = true;
            Utilities.RestService.Response response;
            if (SelectedItem.Id == 0)
            {
                SelectedItem.CreatedAt = DateTimeOffset.Now;
                SelectedItem.UpdatedAt = DateTimeOffset.Now;
                response = await apiService.Post("countries", SelectedItem, false);
            }
            else
            {
                SelectedItem.UpdatedAt = DateTimeOffset.Now;
                response = await apiService.Patch("countries", SelectedItem,false );
            }
            IsRunning = false;

            if (response.Is_Error)
            {
                var error = JsonConvert.DeserializeObject<ErrorModel>(response.Message);
                await userDialogs.AlertAsync(new AlertConfig { Message = error.ErrorDescription, OkText = "Ok", Title = "Error" });
                return;
            }

            await navigationService.GoBackAsync();

        }        

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("Country"))
            {
                SelectedItem = parameters.GetValue<Country>("Country");
                Action = "Edit";
            }
        }
    }
}
