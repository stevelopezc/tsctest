
using System;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Prism;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using TCSTest.Data;
using TCSTest.Infrastructure;
using TCSTest.Services;

namespace TCSTest.ViewModels
{
    public class MainViewModel : TCSTestAppViewModelBase
    {
        private string _UserID;
        private string _Password;
        private bool _RememberMe;
        private readonly INavigationService navigationService;
        private readonly IUserDialogs userDialogs;

        public DelegateCommand LogInCommand { get; set; }
        public string UserID { get => _UserID; set => SetProperty(ref _UserID , value); }
        public string Password { get => _Password; set => SetProperty(ref _Password , value); }
        public bool RememberMe { get => _RememberMe; set => SetProperty(ref _RememberMe , value); }

        public MainViewModel(INavigationService navigationService, IUserDialogs userDialogs, ApiService apiService) : base(navigationService, userDialogs, apiService)
        {
            LogInCommand = new DelegateCommand(async () => await loginCommand());
            this.navigationService = navigationService;
            this.userDialogs = userDialogs;
        }

        private async Task loginCommand()
        {
            if (string.IsNullOrEmpty(UserID))
            {
                await userDialogs.AlertAsync(new AlertConfig { Title = "Error", Message = "Invalid user Id", OkText="Ok" });
                return;
            }

            if (string.IsNullOrEmpty(Password))
            {
                await userDialogs.AlertAsync(new AlertConfig { Title = "Error", Message = "Invalid Password", OkText = "Ok" });
                return;
            }

            if (UserID!= "test@domain.com" ||
                Password != "abc123")
            {
                await userDialogs.AlertAsync(new AlertConfig { Title = "Error", Message = "Invalid User or Password", OkText = "Ok" });
                return;
            }

            if (RememberMe)
            {
                Sesion sesion = new Sesion
                {
                    UserConnected = UserID,
                    LoginDate = DateTimeOffset.Now
                };

                context.Write(() =>
                {
                    context.RemoveAll<Sesion>();
                    context.Add(sesion, true);
                });
            }

            await navigationService.NavigateAsync("/Countries");
        }
    }
}
