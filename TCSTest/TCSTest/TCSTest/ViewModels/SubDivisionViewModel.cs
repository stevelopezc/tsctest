using System;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Prism;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using TCSTest.Infrastructure;
using TCSTest.Models;
using TCSTest.Services;

namespace TCSTest.ViewModels
{
    public class SubDivisionViewModel : TCSTestAppViewModelBase
    {
        private SubDivision _SelectedItem;
        private string _Action;
        private readonly INavigationService navigationService;
        private readonly IUserDialogs userDialogs;
        private readonly ApiService apiService;
        private int CountryId;

        public DelegateCommand CancelCommand { get; set; }
        public DelegateCommand SaveCommand { get; set; }
        public SubDivision SelectedItem { get => _SelectedItem; set => SetProperty(ref _SelectedItem, value); }
        public string Action { get => _Action; set => SetProperty(ref _Action, value); }

        public SubDivisionViewModel(INavigationService navigationService, IUserDialogs userDialogs, ApiService apiService) : base(navigationService, userDialogs, apiService)
        {
            SelectedItem = new SubDivision();
            Action = "New";
            CancelCommand = new DelegateCommand(async () => await cancel());
            SaveCommand = new DelegateCommand(async () => await save());
            this.navigationService = navigationService;
            this.userDialogs = userDialogs;
            this.apiService = apiService;
        }

        private async Task save()
        {
            if (string.IsNullOrEmpty(SelectedItem.Name))
            {
                await userDialogs.AlertAsync(new AlertConfig { Message = "Invalid Name", OkText = "Ok", Title = "Error" });
                return;
            }

            if (string.IsNullOrEmpty(SelectedItem.Code))
            {
                await userDialogs.AlertAsync(new AlertConfig { Message = "Invalid Code", OkText = "Ok", Title = "Error" });
                return;
            }

            IsRunning = true;
            Utilities.RestService.Response response;
            if (SelectedItem.Id == 0)
            {
                SelectedItem.CreatedAt = DateTimeOffset.Now;
                SelectedItem.UpdatedAt = DateTimeOffset.Now;
                response = await apiService.Post($"countries/{CountryId}/subdivisions", SelectedItem, false);
            }
            else
            {
                SelectedItem.UpdatedAt = DateTimeOffset.Now;
                response = await apiService.Patch($"countries/{CountryId}/subdivisions", SelectedItem, false);
            }
            IsRunning = false;

            if (response.Is_Error)
            {
                var error = JsonConvert.DeserializeObject<ErrorModel>(response.Message);
                await userDialogs.AlertAsync(new AlertConfig { Message = error.ErrorDescription, OkText = "Ok", Title = "Error" });
                return;
            }

            await navigationService.GoBackAsync();

        }

        private async Task cancel()
        {
            await navigationService.GoBackAsync(animated: true);
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("SubDivision"))
            {
                SelectedItem = parameters.GetValue<SubDivision>("SubDivision");
                Action = "Edit";
            }
            if (parameters.ContainsKey("Country"))
                CountryId = parameters.GetValue<int>("Country");
        }
    }
}
