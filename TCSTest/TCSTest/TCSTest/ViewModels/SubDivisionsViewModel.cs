using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Prism;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using TCSTest.Infrastructure;
using TCSTest.Models;
using TCSTest.Services;

namespace TCSTest.ViewModels
{
    public class SubDivisionsViewModel : TCSTestAppViewModelBase
    {
        private readonly INavigationService navigationService;
        private readonly IUserDialogs userDialogs;
        private readonly ApiService apiService;
        private Country Country;
        private string _CountryName;

        private SubDivision _SelectedSubDivision;
        private bool _IsSelected;

        public DelegateCommand NewCommand { get; set; }
        public DelegateCommand EditCommand { get; set; }
        public DelegateCommand DeleteCommand { get; set; }

        private ObservableCollection<SubDivision> _SubDivisionsList;
        public SubDivision SelectedSubDivision { get => _SelectedSubDivision; set => SetProperty(ref _SelectedSubDivision , value); }
        public bool IsSelected { get => _IsSelected; set => SetProperty(ref _IsSelected, value); }
        public SubDivisionsViewModel(INavigationService navigationService, IUserDialogs userDialogs, ApiService apiService) : base(navigationService, userDialogs, apiService)
        {
            this.navigationService = navigationService;
            this.userDialogs = userDialogs;
            this.apiService = apiService;

            NewCommand = new DelegateCommand(async () => await GoToNew());
            EditCommand = new DelegateCommand(async () => await GoToEdit());
            DeleteCommand = new DelegateCommand(async () => await Delete());

        }

        public ObservableCollection<SubDivision> SubDivisionsList { get => _SubDivisionsList; set => SetProperty(ref _SubDivisionsList , value); }
        public string CountryName { get => _CountryName; set => SetProperty(ref  _CountryName , value); }

        public async override void OnNavigatedTo(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("Country"))
            {
                Country = parameters.GetValue<Country>("Country");
                CountryName = Country.Name;
            }

            await LoadData();
        }

        private async Task LoadData()
        {
            if (!IsConnected)
            {
                userDialogs.Toast(new ToastConfig("There was an error getting data").SetDuration(TimeSpan.FromSeconds(30)).SetPosition(ToastPosition.Bottom));
                return;
            }

            IsRunning = true;
            var response = await apiService.Get<ResponseSubDivisionAPIModel>($"countries/{Country.Id}/subdivisions", false);
            IsRunning = false;

            if (response.Is_Error)
            {
                var error = JsonConvert.DeserializeObject<ErrorModel>(response.Message);
                userDialogs.Toast(new ToastConfig(error.ErrorDescription).SetDuration(TimeSpan.FromSeconds(30)).SetPosition(ToastPosition.Bottom));
                return;
            }

            SubDivisionsList = new ObservableCollection<SubDivision>((response.Result as ResponseSubDivisionAPIModel).Data);
        }

        public async Task GoToEdit()
        {
            NavigationParameters navParameters = new NavigationParameters
            {
                { "SubDivision", SelectedSubDivision },
                {"Country", Country.Id }
            };
            await navigationService.NavigateAsync("SubDivision", navParameters);
        }

        public async Task GoToNew()
        {
            NavigationParameters navParameters = new NavigationParameters
            {
                {"Country", Country.Id }
            };
            await navigationService.NavigateAsync("SubDivision", navParameters);
        }

        public async Task Delete()
        {
            var confirm = await userDialogs.ConfirmAsync(new ConfirmConfig { Message = "Are you sure to Delete this SubDivision?", OkText = "Ok", CancelText = "Cancel" });
            if (confirm)
            {
                if (!IsConnected)
                {
                    userDialogs.Toast(new ToastConfig("There was an error getting data").SetDuration(TimeSpan.FromSeconds(30)).SetPosition(ToastPosition.Bottom));
                    return;
                }

                IsRunning = true;
                var response = await apiService.Delete($"countries/{Country.Id}/subdivisions", false, SelectedSubDivision);
                IsRunning = false;

                if (response.Is_Error)
                {
                    var error = JsonConvert.DeserializeObject<ErrorModel>(response.Message);
                    await userDialogs.AlertAsync(new AlertConfig { Message = error.ErrorDescription, OkText = "Ok", Title = "Error" });
                    return;
                }

                await LoadData();
            }
        }
    }
}
