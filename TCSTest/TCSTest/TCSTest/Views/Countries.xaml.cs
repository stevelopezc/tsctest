using TCSTest.ViewModels;
using Xamarin.Forms;

namespace TCSTest.Views
{
    public partial class Countries : ContentPage
    {
        public Countries()
        {
            InitializeComponent();
        }

        

        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var viewModel = ((CountriesViewModel)BindingContext);
            viewModel.IsSelected = true;
            viewModel.SelectedCountry = e.SelectedItem as TCSTest.Models.Country;
        }
    }
}