using TCSTest.ViewModels;
using Xamarin.Forms;

namespace TCSTest.Views
{
    public partial class SubDivisions : ContentPage
    {
        public SubDivisions()
        {
            InitializeComponent();
        }

        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var viewModel = ((SubDivisionsViewModel)BindingContext);
            viewModel.IsSelected = true;
            viewModel.SelectedSubDivision = e.SelectedItem as TCSTest.Models.SubDivision;
        }
    }
}